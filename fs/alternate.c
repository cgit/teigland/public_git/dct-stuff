/******************************************************************************
*******************************************************************************
**
**    Copyright 2001 Sistina Software, Inc.
**
**    This is free software released under the GNU General Public License.
**    There is no warranty for this software.  See the file COPYING for
**    details.
**
*******************************************************************************
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#define die(fmt, args...) \
{ \
  fprintf(stderr, "%s: ", prog_name); \
  fprintf(stderr, fmt, ##args); \
  exit(EXIT_FAILURE); \
}

#define do_lseek(fd, off) \
{ \
  if (lseek((fd), (off), SEEK_SET) != (off)) \
    die("bad seek: %s on line %d of file %s\n", \
        strerror(errno),__LINE__, __FILE__); \
}

#define do_read(fd, buff, len) \
{ \
  if (read((fd), (buff), (len)) != (len)) \
    die("bad read: %s on line %d of file %s\n", \
        strerror(errno), __LINE__, __FILE__); \
}

#define do_write(fd, buff, len) \
{ \
  if (write((fd), (buff), (len)) != (len)) \
    die("bad write: %s on line %d of file %s\n", \
        strerror(errno), __LINE__, __FILE__); \
}

#define BLOCK_SIZE (512)

char *prog_name;

unsigned int wait;
unsigned int delay = 0;

int main(int argc, char *argv[])
{
	int cont = 1;
	int optchar;
	int fd;
	char *filename;
	char buf[BLOCK_SIZE];
	unsigned long long offset;
	unsigned long long num, last_num = 0;
	unsigned int id, clients;
	unsigned long long skip = 0;
	unsigned int iterations = 0;
	unsigned int delay = 0;

	prog_name = argv[0];

	while (cont) {
		optchar = getopt(argc, argv, "i:s:");
		switch (optchar) {
		case 'i':
			iterations = atoi(optarg);
			break;
		case 's':
			delay = atoi(optarg);
			break;
		case 'h':
			die("[-i iterations] [-s usleep] filename offset id clients\n");
		case EOF:
			cont = 0;
			break;
		}
	}

	if (argc - optind < 4)
		die("[-i iterations] [-s usleep] filename offset id clients\n");

	filename = argv[optind];
	offset = atoll(argv[optind + 1]);
	id = atoi(argv[optind + 2]);
	clients = atoi(argv[optind + 3]);

	fd = open(filename, O_RDWR | O_CREAT, 0644);
	if (fd < 0)
		die("can't open file %s: %s\n", argv[1], strerror(errno));

	for (;;) {
		do_lseek(fd, offset);

		do_read(fd, buf, BLOCK_SIZE);

		num = atoll(buf);
		if (num % clients == id) {
			num++;
			sprintf(buf, "%llu\n", num);
			printf("%llu %llu\n", num, skip);

			if (last_num && last_num + clients != num)
				die("bad\n");

			do_lseek(fd, offset);

			do_write(fd, buf, BLOCK_SIZE);

			if (iterations && num >= iterations)
				break;

			last_num = num;
			skip = 0;
		} else {
			skip++;
		}

		if (delay)
			usleep(delay);
	}

	close(fd);

	exit(EXIT_SUCCESS);
}

