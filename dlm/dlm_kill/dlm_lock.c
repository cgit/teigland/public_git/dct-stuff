/*
 * Copyright (c) 2010 David Teigland
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License V2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it would be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <sys/types.h>
#include <sys/uio.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>

#include "libdlm.h"

static int ast_called = 0;
static int quit;

static int do_release(void)
{
	static dlm_lshandle_t *dh;

	dh = dlm_create_lockspace("default", 0600);
	if (!dh) {
		printf("dlm_lock create lockspace error\n");
		return -1;
	}

	dlm_release_lockspace("default", dh, 1);
	return 0;
}

void sigterm_handler(int sig)
{
	quit = 1;
}

static void bast_fn(void *arg)
{
	printf("dlm_lock bast\n");
}

static void ast_fn(void *arg)
{
	printf("dlm_lock ast\n");
	ast_called = 1;
}

static int poll_for_ast(void)
{
	struct pollfd pfd;

	pfd.fd = dlm_get_fd();
	pfd.events = POLLIN;

	while (!ast_called) {
		if (poll(&pfd, 1, 0) < 0) {
			perror("poll");
			return -1;
		}
		dlm_dispatch(pfd.fd);
	}
	ast_called = 0;
	return 0;
}

int do_lock(char *name, struct dlm_lksb *lksb)
{
	int len = strlen(name);
	int rv;

	printf("dlm_lock request NL ...\n");
	fflush(stdout);

	rv = dlm_lock(LKM_NLMODE, lksb, 0, name, len, 0,
		      ast_fn, lksb, bast_fn, NULL);
	if (rv < 0) {
		printf("dlm_lock request NL done error %d %d", rv, errno);
		return -1;
	}

	poll_for_ast();

	printf("dlm_lock request NL done status %d lkid %x\n",
	       lksb->sb_status, lksb->sb_lkid);

	printf("dlm_lock convert EX ...\n");
	fflush(stdout);

	rv = dlm_lock(LKM_EXMODE, lksb, LKF_CONVERT, name, len, 0,
		      ast_fn, lksb, bast_fn, NULL);
	if (rv < 0) {
		printf("dlm_lock convert EX done error %d %d", rv, errno);
		return -1;
	}

	poll_for_ast();

	printf("dlm_lock convert EX done status %d\n", lksb->sb_status);
	fflush(stdout);

	return 0;
}

int main(int argc, char *argv[])
{
	struct dlm_lksb lksb;
	char name[32];
	int i, count;

	signal(SIGTERM, sigterm_handler);

	printf("dlm_lock pid %d\n", getpid());

	if (argc > 1 && !strcmp(argv[1], "release")) {
		do_release();
		return 0;
	}

	if (argc < 2)
		count = 1;
	else
		count = atoi(argv[1]);

	for (i = 0; i < count; i++) {
		memset(name, 0, sizeof(name));
		sprintf(name, "dlm_lock%d", i);
		printf("dlm_lock resource %s\n", name);
		/* too lazy to properly use separate lksb for each */
		memset(&lksb, 0, sizeof(lksb));
		do_lock(name, &lksb);
	}

	while (!quit)
		pause();

	return 0;
}

