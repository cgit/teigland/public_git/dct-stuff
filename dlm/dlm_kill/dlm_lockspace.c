/*
 * Copyright (c) 2010 David Teigland
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License V2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it would be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <sys/types.h>
#include <sys/uio.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>

#include "libdlm.h"

int quit;

void sigterm_handler(int sig)
{
	printf("sigterm\n");
	quit = 1;
}

int main(int argc, char *argv[])
{
	dlm_lshandle_t ls;
	int rv;

	signal(SIGTERM, sigterm_handler);

	printf("pid %d\n", getpid());
	printf("creating default lockspace...\n");

	ls = dlm_create_lockspace("default", 0444);
	if (!ls) {
		fprintf(stderr, "dlm_create_lockspace error %d\n", errno);
		return -1;
	}
	printf("done\n");

	while (!quit)
		pause();

	printf("releasing default lockspace...\n");

	rv = dlm_release_lockspace("default", ls, 1);
	if (rv) {
		fprintf(stderr, "dlm_release_lockspace error %d %d\n", rv, errno);
		return -1;
	}
	printf("done\n");

	return 0;
}

