Name:           cpgx
Version:        1.2
Release:        1%{?dist}
Summary:        corosync cpg test

Group:          QA
License:        GPL
Buildroot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Source0:        cpgx-%{version}.tar.gz

%if 0%{?rhel} == 5
BuildRequires:	openais-devel
%else
BuildRequires:	corosync-devel
%endif

%description
This program tests the virtual synchrony guarantees of corosync and cpg.

%prep
%setup -q


%build
make


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
/usr/bin/cpgx


%changelog
* Tue Jun 09 2009 David Teigland <teigland@redhat.com> 1.2-1
- add -w option

* Tue Jun 09 2009 David Teigland <teigland@redhat.com> 1.1-1
- restart improvements

* Tue Jun 02 2009 David Teigland <teigland@redhat.com> 1.0-1
- Initial packaging
